#!/usr/bin/env bash
pipenv run pyinstaller --icon=./icons/icon.ico --onefile -n GradesDigitalSigning main.py --hidden-import=tkinter
