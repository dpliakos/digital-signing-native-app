#!/usr/bin/env python3
import asyncio
import websockets
import json
from .TokenManager import TokenManager
from .Interfaces import BasicInterfaceController, GraphicalInterface
from .CommandRouter import CommandRouter

# interface = BasicInterfaceController()
interface = GraphicalInterface()
token_manager = TokenManager(interface.get_library_location)
command_router = CommandRouter(token_manager, interface)

async def request_loop(websocket, path):
    allowed = interface.ask_permission(websocket)
    if allowed != True:
        await websocket.send(json.dumps({'error': allowed}))
        return websocket.close(1000)
    try:
        async for message in websocket:
            print('> ', message)
            try:
                input_payload = json.loads(message)
                output_payload = command_router.route(input_payload)
            except json.decoder.JSONDecodeError:
                output_payload = {'error': 'Invalid payload JSON.'}
            await websocket.send(json.dumps(output_payload))
            print('< ', json.dumps(output_payload))
    except websockets.exceptions.ConnectionClosed:
        pass # disconnect
    finally:
        pass # after something bad

def main():
    signing_server = websockets.serve(request_loop, 'localhost', 8765) # port number is here

    async def looper():
        await asyncio.gather(signing_server, interface.loop())

    asyncio.get_event_loop().run_until_complete(looper())
    asyncio.get_event_loop().run_forever()

if __name__ == "__main__":
    main()