# Native Signing App
This app is used to offer cross-platform application agnostic signing of payloads.

Disclaimer: I removed the lock file for environment inconsistencies across operating systems. Please build with caution.

## Building native executables
NOTE: Native Executables are self contained.
They DO NOT need python installed on the machine.

## Build Environment Requirements

### Windows
1. Install Python 3.6+ and pip
2. Install [Swig](http://www.swig.org/download.html) & add to path
3. Install [Buildtools C++](https://go.microsoft.com/fwlink/?LinkId=691126)
4. `pip install --user pipenv`
5. `pipenv install`
6. `.\scripts\build_windows.bat`

### Linux
1. `sudo apt install python3-pip`
2. `sudo apt install build-essential gcc python3-dev python3-setuptools swig python3-tk`
3. `pip3 install --user pipenv`
4. add `export PATH="$HOME/.local/bin:$PATH"` to the end of your `.bashrc`
5. rename `LinuxPipfile.lock` to `Pipfile.lock`
6. `pipenv sync`
7. `./scripts/build_linux.sh`

### macOS
1. `brew install python3`
2. `pip3 install --user pipenv`
3. `brew install swig`
4. `pipenv install`
5. `sh ./scripts/build_macos.sh`


## Development Environment Requirements

### Windows Environment
1. Install python 3.6+ and pip & add to path
2. [Swig](http://www.swig.org/download.html) & add to path
3. [Buildtools C++](https://go.microsoft.com/fwlink/?LinkId=691126)
4. `pip install --user pipenv`
5. `pipenv install`

### Linux Environment
1. `sudo apt install python3-pip`
2. `sudo apt install build-essential gcc python3-dev python3-setuptools swig python3-tk`
3. `pip3 install --user pipenv`
4. add `export PATH="$HOME/.local/bin:$PATH"` to the end of your `.bashrc`
5. rename `LinuxPipfile.lock` to `Pipfile.lock`
6. `pipenv sync`

### macOS Environment
1. `brew install python3`
2. `pip3 install --user pipenv`
3. `brew install swig`
4. `pipenv install`

## Development
1. `pipenv install`
2. `pipenv run python main.py`

## Testing
IMPORTANT: the tests require a crypto-token device.
1. `pipenv run python -m unittest discover`

## Examples

### Basic example
Simply serve the index.html file

### Angular example
1. `npm i`
2. `npm run start`
3. Browse to http://localhost:4200

### Python Example
1. `pipenv run ./examples/python/client.py`