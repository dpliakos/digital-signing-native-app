import { Component, OnInit } from '@angular/core';
import WebSocketAsPromised = require("websocket-as-promised")

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular'
  socket_open = false
  error = ''
  slots_info: any = {}
  certs_info: any = {}
  selected_slot = ''
  selected_cert = ''
  selected_source_encoding = 'plain'
  selected_destination_encoding = 'hex'
  message = 'hello'
  output = '...'
  private ws : WebSocketAsPromised;

  /**
   * ===============================================================
   * Lifecycle methods
   * ===============================================================
   */

  constructor () {
    this.ws = new WebSocketAsPromised('ws://127.0.0.1:8765?app=Test_App', {
      packMessage: data => JSON.stringify(data),
      unpackMessage: (data : string) => JSON.parse(data),
      attachRequestId: (data, requestId) => Object.assign({request_id: requestId}, data),
      extractRequestId: data => data && data.request_id
    })

    this.ws.onClose.addListener(() => this.socket_open = false)
  }

  async ngOnInit() {
    console.log('onInit')
  }

  /**
   * ===============================================================
   * Utilities
   * ===============================================================
   */

  async openSocket() {
    try {
      await this.ws.open()
      this.socket_open = true
    } catch (error) {
      this.error = error
    }
  }

  objectKeys = Object.keys

  JSONencode = JSON.stringify

  async sendRequest(payload : any) {
    if (this.socket_open) {
      this.error = ''
      const response = await this.ws.sendRequest(payload)
      console.log(response)
      if (response.error) {
        throw new Error(response.error)
      }
      return response
    }
    throw Error('Websocket not open.')
  }

  resetSelectedCert() {
    this.selected_cert = ''
  }

  /**
   * ===============================================================
   * Commands
   * ===============================================================
   */

  async listSlots() {
    try {
      const response = await this.sendRequest({
        command: 'list_slots'
      })
      this.slots_info = response.payload
    } catch (error) {
      this.error = error.message
    }
  }

  async openSession() {
    try {
      const response = await this.sendRequest({
        command: 'open_session',
        slot_serial: this.selected_slot
      })
      this.slots_info[this.selected_slot].open = true
    } catch (error) {
      this.error = error.message
    }
  }

  async loginSession() {
    try {
      const response = await this.sendRequest({
        command: 'login_session',
        slot_serial: this.selected_slot
      })
      this.slots_info[this.selected_slot].logged = true
    } catch (error) {
      this.error = error.message
    }
  }

  async listCerts() {
    try {
      const response = await this.sendRequest({
        command: 'list_certs',
        slot_serial: this.selected_slot
      })
      this.certs_info = response.payload
      this.slots_info[this.selected_slot].certificates = response.payload
    } catch (error) {
      this.error = error.message
    }
  }

  async signWithCert() {
    try {
      const response = await this.sendRequest({
        command: 'sign_with_cert',
        slot_serial: this.selected_slot,
        cert_id: this.selected_cert,
        message: this.message,
        source_encoding: this.selected_source_encoding,
        destination_encoding: this.selected_destination_encoding
      })
      this.output = response.payload.signature
    } catch (error) {
      this.error = error.message
    }
  }

  /**
   * ===============================================================
   * Getters
   * ===============================================================
   */

  get selectedSlotOpen() {
    return this.selected_slot !== '' && this.slots_info[this.selected_slot].open
  }

  get selectedSlotLogged() {
    return this.selected_slot !== '' && this.slots_info[this.selected_slot].logged
  }

  get selectedCert() {
    return this.selected_cert !== ''
  }

  get signFlag() {
    return this.socket_open
    && this.selectedSlotOpen
    && this.selectedSlotLogged
    && this.selectedCert
    && this.message !== ''
  }

  get slotSelectedSlotInfo() {
    if (this.selected_slot) {
      return this.slots_info[this.selected_slot]
    }
    return false
  }

  get slotSelectedCertInfo() {
    if (this.slotSelectedSlotInfo && Object.keys(this.slotSelectedSlotInfo.certificates).length > 0 && this.selected_cert) {
      return this.slotSelectedSlotInfo.certificates[this.selected_cert]
    }
    return false
  }
}
