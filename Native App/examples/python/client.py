import asyncio
import websockets
import json

async def hello():
    async with websockets.connect(
            'ws://localhost:8765?app=Test_App') as websocket:

        commands = [
            {'command': 'help'},
            {'command': 'list_slots'}, # list slots
            {'command': 'list_certs', 'slot_serial': '021f60d0'}, # session must be open
            {'command': 'open_session', 'slot_serial': '021f60d0'}, # open session
            {'command': 'list_certs', 'slot_serial': '021f60d0'}, # session must be logged
            {'command': 'login_session', 'slot_serial': '021f60d0'}, # login session
            {'command': 'login_session', 'slot_serial': '021f60d0'}, # try to login again and see logged in message
            {'command': 'list_certs', 'slot_serial': '021f60d0'}, # list available certificates
            {'command': 'list_certs', 'slot_serial': '021f60da'}, # incorrect slot hash
            {'command': 'list_slots'}, # list slots and see that have certificates info
            {
                'command': 'sign_with_cert',
                'slot_serial': '021f60d0',
                'cert_id': '228.10.12.161.68.68.63.246',
                'message': 'iqqcKxuFJeb6TAg2Tm3E9DAOySw=',
                'source_encoding': 'base64',
                'destination_encoding': 'base64'
            }
        ]

        for command in commands:
            temp = json.dumps(command)
            await websocket.send(temp)
            print(f"< {temp}")
            payload = await websocket.recv()
            print(f'> {payload}')

asyncio.get_event_loop().run_until_complete(hello())