## sis.auth.gr Chrome Cert Signing Extension

Tο extension για λόγους development μπορεί
να τοποθετηθεί σε ενα folder τοπικά και μεσα στον chrome στο
chrome://extensions να φορτωθεί απο το "Load Unpacked extension..."

Προαπαιτούμενο για το extension ειναι να εχει εγκατασταθεί πρώτα
το docsingapp.exe με τον custom installer που τον κανουμε build με το project που βρίσκεται
στο network drive W:\SERV\APP\egram\install_token_new\

H διαδικασία του publishing του extension στο webstore περιλαμβανει
ενέργειες απο την μεριά του Δημήτρη (packing και signing)

Με κάθε publish το version στο manifest.json πρεπει να αυξάνεται.

Επίσης αλλάζει το Extension ID στο webstore. Σημείωση οτι το published Extension ID πρεπει να βρίσκεται
μεσα στο W:\SERV\APP\egram\install_token_new\prerequisites\nativeapp\manifest.json για να γίνει σωστά
η εγκατάσταση του docsignapp.exe (ΣΗΜΕΙΩΣΗ: Ειναι ηδη το σωστό απλα το αναφέρω για να ειναι καταγεγραμμένο).



